package Lab3;
public class Aims {
	public static void main(String[] args) {
		////TODO 
		Order anOrder = new Order();
		// Creat a new dvd object and set the field
		anOrder.setQtyOrdered(0);
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setCost(19.95f);
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		// add the dvd to the order
		if(anOrder.addDigitalVideoDisc(dvd1)) {
			System.out.println("Order Success");
		}else {
			System.out.println("Order Fail");
		}
		
		//dvd2
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("abc");
		dvd2.setCategory("Animation");
		dvd2.setCost(18.99f);
		dvd2.setDirector("Roger Allers");
		dvd2.setLength(90);
		if(anOrder.addDigitalVideoDisc(dvd2)) {
			System.out.println("Order Success");
		}else {
			System.out.println("Order Fail");
		}
		//dvd3
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("abd");
		dvd3.setCategory("Animation");
		dvd3.setCost(24.95f);
		dvd3.setDirector("Roger Allers");
		dvd3.setLength(124);
		if(anOrder.addDigitalVideoDisc(dvd3)) {
			System.out.println("Order Success");
		}else {
			System.out.println("Order Fail");
		}
		
		if(anOrder.removeDigitalVideoDisc(dvd1)) {
			System.out.println("Delete Complete");
		}else {
			System.out.println("Delete Fail");
		}
		
		System.out.print("Total Cost is: ");
		System.out.println(anOrder.totalCost());
}
}