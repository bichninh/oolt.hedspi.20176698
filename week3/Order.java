package Lab3;
public class Order {
public static final int MAX_NUMBER_ORDERED =10;
private DigitalVideoDisc itemsOrdered[]= new DigitalVideoDisc [MAX_NUMBER_ORDERED];
private int qtyOrdered;
public int getQtyOrdered() {
	return qtyOrdered;
}
public void setQtyOrdered(int qtyOrdered) {
	this.qtyOrdered = qtyOrdered;
}
public boolean addDigitalVideoDisc(DigitalVideoDisc disc) {
	if(this.qtyOrdered > MAX_NUMBER_ORDERED) return false;
	else {
		itemsOrdered[qtyOrdered] = disc;
		this.qtyOrdered ++;
		return true;
	}
}
public boolean removeDigitalVideoDisc( DigitalVideoDisc disc) { 
	int a, i;
	boolean result = false;
	for( a = i = 0; i < this.qtyOrdered ; i++) {
		if(this.itemsOrdered[i].getTitle() != disc.getTitle()) {
			this.itemsOrdered[a] = this.itemsOrdered[i];
			a++;
		}
	}
	if(this.qtyOrdered != a) {
		this.setQtyOrdered(a);
		result = true;
	}
	return result;
	
 }
public float totalCost() {
	float totalCost = 0;
	int i;
	for(i = 0; i < this.qtyOrdered; i++) {
		totalCost += itemsOrdered[i].getCost();
	}
	return totalCost;
}
}